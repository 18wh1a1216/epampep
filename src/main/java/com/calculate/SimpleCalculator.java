package com.calculate;
import java.util.Scanner;

public class SimpleCalculator {

	  public static void main(String[] args) {

	    Scanner reader = new Scanner(System.in);
	    System.out.print("Enter two numbers: ");

	    double first = reader.nextDouble();
	    double second = reader.nextDouble();

	    System.out.print("Enter an operator (+, -, *, /): ");
	    char operator = reader.next().charAt(0);

	    double result;

	    switch (operator) {
	      case '+':
	        result = add(first,second);
	        break;

	      case '-':
	    	result = subtract(first,second);
	        break;

	      case '*':
	    	result = multiply(first,second);
	        break;

	      case '/':
	    	result = divide(first,second);
	        break;

	      // operator doesn't match any case constant (+, -, *, /)
	      default:
	        System.out.println("Error! " + operator + " is not supported");
	        return;
	    }

	    System.out.println(first + " " + operator + " " + second + " = " + result);
	  }

	private static double divide(double first, double second) {
		// TODO Auto-generated method stub
		return (first / second);
		
	}

	private static double multiply(double first, double second) {
		// TODO Auto-generated method stub
		return (first * second);
		
	}

	private static double subtract(double first, double second) {
		// TODO Auto-generated method stub
		return (first - second);
	}

	private static double add(double first, double second) {
		// TODO Auto-generated method stub
		return (first + second);
	}
	}