package com.dry;

public class Animal {
	/* common logic implementation */
    public void eatFood() {
        System.out.println("Eating food...");
    }
    public static void main(String[] args) {
    	Dog dg = new Dog();
    	Cat ct = new Cat();
    	dg.eatFood();
    	ct.eatFood();
    	

    }
}

