package com.dry;

public class Cat extends Animal {
	/* unique logic implementation */
    public void meow() {
        System.out.println("Meow! *purrs*");
    }
}