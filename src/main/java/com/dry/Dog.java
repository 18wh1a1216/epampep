package com.dry;

public class Dog extends Animal {
	/* unique logic implementation */
    public void woof() {
        System.out.println("Woof! *wags tail*");
    }
    
}