package com.kiss;

import com.dry.Cat;
import com.dry.Dog;

public class DayOfWeek {
	public String weekDay(int day) throws Exception {
		/* use array-indexing technique instead of switch case to keep the logic simple*/
		
		if ((day < 1) || (day > 7)) throw new Exception("day must be in range 1 to 7");
		String[] days = {"Monday", "Tuesday","Wednesday","Thursday", "Friday", "Saturday", "Sunday"};
		return days[day - 1];
}
	 public static void main(String[] args) {
	    	DayOfWeek dow = new DayOfWeek();
	    	try {
				System.out.println(dow.weekDay(3));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	    }
}
