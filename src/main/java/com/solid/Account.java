package com.solid;

public abstract class Account {
	
	/* In order to implement SINGLE RESPONSIBILITY PRINCIPLE, use account class to represent 
	 * account entity and include all the members & responsibilities of any bank account here.*/
    private String accountNumber;
    private String accountName;
    private String type;
    /* In order to implement DEPENDENCY INVERSION PRINCIPLE, make account an abstract class 
     * with abstract method called TransactionLimit, this gives us an abstract 
     * layer of separation between the subclasses
     * */
    abstract String transactionLimit();
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
    
}
