package com.solid;

public class CurrentAcc extends WithdrawableAcc {
	/* In order to implement LISKOV SUBSTITUTION PRINCIPLE, extend WithdrawalAcc class 
	 * from current account so that the both the accounts which can be used for withdrawable 
	 * are present under one class i.e. WithdrawableAcc. 
	 */
	
	String transactionLimit() {
		return "unlimited number of transactions for Current Account";
	}
public static void main(String[] args) {
	CurrentAcc cacc = new CurrentAcc();
	cacc.setType("current");
	System.out.println(cacc.getType());
	System.out.println(cacc.transactionLimit());
    }
    
    
}

