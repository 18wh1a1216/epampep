package com.solid;


public class SavingsAcc extends WithdrawableAcc {
	/* In order to implement INTERFACE SEGREGATION PRINCIPLE, create SavingsAccount separately 
	 * so that customers who would not like to make transactions on daily basis can use this account
	 * instead of using a current account that has features which are not useful to them.
	 */
	/* In order to implement OPEN CLOSED PRINCIPLE, extend WithdrawalAccount class from savings account 
	 * so that the flow & features for savings account can be extended to meet 
	 * the expectations of SavingsAcc class, but no modifications can be made to Account class. */
String transactionLimit() {
	return "3 to 5 transactions per month for Savings Account";
}
public static void main(String[] args) {
	SavingsAcc sacc = new SavingsAcc();
	sacc.setType("savings");
	System.out.println(sacc.getType());
	System.out.println(sacc.transactionLimit());
    }
    
    
}

