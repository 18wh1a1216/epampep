package com.yagni;



public class Human {
	/* In order to implement YAGNI perform only those methods/functionalities that 
	 * are needed in the design process. */
	public void acquireNutrition() {
    System.out.println("Humans must eat food & drink water to survive - required functionality");
}
	public void breatheAir() {
	    System.out.println("Humans must breathe air that contains oxygen to survive. - required functionality");
	}
public static void main(String[] args) {
	Human hm = new Human();
	hm.acquireNutrition();
	hm.breatheAir();
	

}
}
